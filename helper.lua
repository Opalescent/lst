local bindings = require "bindings"

local function exists(path) return os.rename(path, path) end

if not exists("bindings") then
    print("Generating Lua Bindings for C Defines ...")
    bindings.gen_c_lua_bindings()
    print("\nDone")
elseif assert(io.popen("whoami")):read("*all") ~= assert(io.popen("namei -l bindings | tail -1 | awk '{print $2}'")):read("*all") then
    print("You do not have permission to access `/usr/lib/st/bindings` directory, please change permissions or remove the directory.")
    os.exit(0)
end

local config_file = (os.getenv("XDG_CONFIG_HOME") ~= '' and os.getenv("XDG_CONFIG_HOME") or (os.getenv("HOME") .. "/.config")) .. "/lst/config.lua"

if exists(config_file) then
    package.path = package.path .. ";" .. config_file:gmatch("(.+/)")() .. "?.lua"
    return require(config_file:gmatch(".+%/([^%.]+)%..+")())
end