local bindings = {}

bindings.files = {
    {
        "bindings/X11.lua", {
            "/usr/include/X11/cursorfont.h",
            "/usr/include/X11/keysymdef.h",
            "/usr/include/X11/X.h",
        },
    },
}

local function isnumber(s) return tonumber(s) ~= nil or type(s) == "number" end

function bindings.gen_c_lua_bindings()
    -- print("Binding Generation requires sudo to write bindings to files. Please run ")

    os.execute("mkdir -p bindings")

    for _, outFile in pairs(bindings.files) do
        local defines = {}

        for _, inFile in pairs(outFile[2]) do
            local contents = assert(io.open(inFile, "rb")):read("*all")

            for line in contents:gmatch("[^\r\n]+") do
                if line:find("^#define") ~= nil then
                    -- Eliminate bad cases...
                    if line:find(" ") == nil then
                        goto continue
                    end

                    local result = {}
                    local nb = 0
                    local lastPos

                    for part, pos in line:gmatch("(.-)[ |\t]+()") do
                        nb = nb + 1
                        result[nb] = part
                        lastPos = pos
                    end
                    -- Handle the last field
                    result[nb + 1] = line:sub(lastPos)

                    local name, value = result[2], result[3]

                    -- If the define was empty or if the define has parameters then continue
                    if value == nil or name:find("%(") or value:sub(1, 1):match("%a") then
                        goto continue
                    end

                    -- Remove an L if it exists since we don't care about longs here
                    value = value:gsub("L", "")
                    -- In the case of a hex or number value
                    if value:sub(1, 2) == "0x" or isnumber(value) then
                        value = tonumber(value)
                    -- In the case if a bit shift operation or function
                    elseif value:sub(1, 1) == "(" then
                        -- If the character after the "(" is an alpha then continue since we don't care about functions
                        if value:sub(2, 2):match("%a") then
                            goto continue
                        end
                        -- Handles left and right bit shifts
                        if value:find("<<") then
                            local n, m = value:gmatch("(%d+)<<(%d+)")()
                            value = (tonumber(n) << tonumber(m))
                        elseif value:find(">>") then
                            local n, m = value:gmatch("(%d+)>>(%d+)")()
                            value = (tonumber(n) >> tonumber(m))
                        end
                    end

                    table.insert(defines, { name, value })
                end
                ::continue::
            end
        end

        local file = assert(io.open(outFile[1], "w+"))
        table.sort(defines, function(lhs, rhs)
            if lhs[2] < rhs[2] then return true
            elseif lhs[2] > rhs[2] then return false
            else
                return lhs[1] < rhs[1]
            end
        end)
        for _, v in pairs(defines) do
            file:write(v[1] .. " = " .. v[2] .. "\n")
        end
        file:close()
    end

    local file = assert(io.open("bindings/lst.lua", "w+"))
    file:write([[XK_NO_MOD = 0
XEMBED_FOCUS_IN = 4
XEMBED_FOCUS_OUT = 5
XK_SWITCH_MOD = 8192

XK_ANY_MOD = 4294967295

SEL_REGULAR = 1
SEL_RECTANGULAR = 2

clipcopy = 0
clippaste = 1
numlock = 2
printscreen = 3
printsel = 4
selpaste = 5
sendbreak = 6
toggleprinter = 7
ttysend = 8
zoom = 9
zoomreset = 10
]])
    file:close()
end

return bindings