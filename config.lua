-- `bindings` is already in scope from `helper.lua`
require "bindings.X11"
require "bindings.lst"

-- termmod must be defined outside of the config table, since it is used in the key table
local termmod = ControlMask | ShiftMask

return {
    --[[
        appearance

        font: see http://freedesktop.org/software/fontconfig/fontconfig-user.html
    --]]
    font = "Liberation Mono:pixelsize=12:antialias=true:autohint=true",
    borderpx = 2,

    --[[
        What program is execed by st depends of these precedence rules:
        1: program passed with -e
        2: utmp option
        3: SHELL environment variable
        4: value of shell in /etc/passwd
        5: value of shell in config.h
    --]]
    shell = "/bin/sh",
    utmp = nil,
    stty_args = "stty raw pass8 nl -echo -iexten -cstopb 38400",

    -- identification sequence returned in DA and DECID
    vtiden = "\x1b[?6c",

    -- Ke r ning / charac ter bounding-bo x   m    ultipliers
    cwscale = 1.0,
    chscale = 1.0,

    --[[
        word delimiter string

        More advanced example: " `'\"()[]{}"
    --]]
    worddelimiters = " ",

    -- selection timeouts (in milliseconds)
    doubleclicktimeout = 300,
    tripleclicktimeout = 600,

    -- alt screens
    allowaltscreen = 1,

    -- frames per second st should at maximum draw to the screen
    xfps = 120,
    actionfps = 30,

    -- blinking timeout (set to 0 to disable blinking) for the terminal blinking attribute
    blinktimeout = 800,

    -- thickness of underline and bar cursors
    cursorthickness = 2,

    -- bell volume. It must be a value between -100 and 100. Use 0 for disabling it
    bellvolume = 0,

    -- default TERM value
    termname = "lst-256color",

    --[[
        spaces per tab

        When you are changing this value, don't forget to adapt the »it« value in
        the st.info and appropriately install the st.info in the environment where
        you use this st version.

    	it#$tabspaces,

        Secondly make sure your kernel is not expanding tabs. When running `stty
        -a` »tab0« should appear. You can tell the terminal to not expand tabs by
        running following command:

    	stty tabs
    --]]
    tabspaces = 8,

    -- Terminal colors (first 16 used in escape sequences)
    colorname = {
        "black",
        "red3",
        "green3",
        "yellow3",
        "blue2",
        "magenta3",
        "cyan3",
        "gray90",

        "gray50",
        "red",
        "green",
        "yellow",
        "#5c5cff",
        "magenta",
        "cyan",
        "white",

        [255] = 0,

        "#cccccc",
        "#555555",
        "#333333",
    },

    --[[
        Default colors (colorname index)

        foreground, background, cursor, reverse cursor
    --]]
    defaultfg = 7,
    defaultbg = 0,
    defaultcs = 256,
    defaultrcs = 257,

    --[[
        Default shape of cursor
        2: Block ("█")
        4: Underline ("_")
        6: Bar ("|")
        7: Snowman ("☃")
    --]]
    cursorshape = 2,

    -- Default columns and rows numbers
    cols = 80,
    rows = 24,

    -- Default color and shape of the mouse cursor
    mouseshape = XC_xterm,
    mousefg = 7,
    mousebg = 0,

    --[[
        Color used to display font attributes when fontconfig selected a font which
        doesn't atch the ones requested
    --]]
    defaultattr = 11,

    --[[
        Force mouse select/shortcuts while mask is active (when MODE_MOUSE is set).
        Note that if you want to use ShiftMask with selmasks, set this to an other
        modifier, set to 0 to not use it.
    --]]
    forcemousemod = ShiftMask,

    -- Internal mouse shortcuts. Beware that overloading Button1 will disable selection.
    mshortcuts = {
        --  mask                 button   func              arg       release
        { XK_ANY_MOD,           Button2, selpaste,       {i = 0},      1 },
        { XK_ANY_MOD,           Button4, ttysend,        {s = "\031"} },
        { XK_ANY_MOD,           Button5, ttysend,        {s = "\005"} },
    },

    -- Internal keyboard shortcuts
    modkey = Mod1Mask,
    termmod = termmod,
    shortcuts = {
        --  mask                 keysym          function         args
        { XK_ANY_MOD,           XK_Break,       sendbreak,      {i =  0} },
        { ControlMask,          XK_Print,       toggleprinter,  {i =  0} },
        { ShiftMask,            XK_Print,       printscreen,    {i =  0} },
        { XK_ANY_MOD,           XK_Print,       printsel,       {i =  0} },
        { termmod,              XK_Prior,       zoom,           {f =  1} },
        { termmod,              XK_Next,        zoom,           {f = -1} },
        { termmod,              XK_Home,        zoomreset,      {f =  0} },
        { termmod,              XK_C,           clipcopy,       {i =  0} },
        { termmod,              XK_V,           clippaste,      {i =  0} },
        { termmod,              XK_Y,           selpaste,       {i =  0} },
        { ShiftMask,            XK_Insert,      selpaste,       {i =  0} },
        { termmod,              XK_Num_Lock,    numlock,        {i =  0} },
    },

    --[[
        Special keys (change & recompile st.info accordingly)

        Mask value:
        * Use XK_ANY_MOD to match the key no matter modifiers state
        * Use XK_NO_MOD to match the key alone (no modifiers)
        appkey value:
        * 0: no value
        * > 0: keypad application mode enabled
        *   = 2: term.numlock = 1
        * < 0: keypad application mode disabled
        appcursor value:
        * 0: no value
        * > 0: cursor application mode enabled
        * < 0: cursor application mode disabled

        Be careful with the order of the definitions because st searches in
        this table sequentially, so any XK_ANY_MOD must be in the last
        position for a key.
    --]]

    --[[
        If you want keys other than the X11 function keys (0xFD00 - 0xFFFF)
        to be mapped below, add them to this array.
    --]]
    mappedkeys = { -1 },

    --[[
        State bits to ignore when matching key or button events. By default,
        numlock (Mod2Mask) and keyboard layout (XK_SWITCH_MOD) are ignored.
    --]]
    ignoremod = Mod2Mask | XK_SWITCH_MOD,

    --[[
        This is the huge key array which defines all compatibility in the Linux
        world. Please make changes wisely.
    --]]
    key = {
        --  keysym             mask           string     appkey  appcursor
        { XK_KP_Home,       ShiftMask,      "\x1b[2J",       0,   -1},
        { XK_KP_Home,       ShiftMask,      "\x1b[1;2H",     0,    1},
        { XK_KP_Home,       XK_ANY_MOD,     "\x1b[H",        0,   -1},
        { XK_KP_Home,       XK_ANY_MOD,     "\x1b[1~",       0,    1},
        { XK_KP_Up,         XK_ANY_MOD,     "\x1bOx",        1,    0},
        { XK_KP_Up,         XK_ANY_MOD,     "\x1b[A",        0,   -1},
        { XK_KP_Up,         XK_ANY_MOD,     "\x1bOA",        0,    1},
        { XK_KP_Down,       XK_ANY_MOD,     "\x1bOr",        1,    0},
        { XK_KP_Down,       XK_ANY_MOD,     "\x1b[B",        0,   -1},
        { XK_KP_Down,       XK_ANY_MOD,     "\x1bOB",        0,    1},
        { XK_KP_Left,       XK_ANY_MOD,     "\x1bOt",        1,    0},
        { XK_KP_Left,       XK_ANY_MOD,     "\x1b[D",        0,   -1},
        { XK_KP_Left,       XK_ANY_MOD,     "\x1bOD",        0,    1},
        { XK_KP_Right,      XK_ANY_MOD,     "\x1bOv",        1,    0},
        { XK_KP_Right,      XK_ANY_MOD,     "\x1b[C",        0,   -1},
        { XK_KP_Right,      XK_ANY_MOD,     "\x1bOC",        0,    1},
        { XK_KP_Prior,      ShiftMask,      "\x1b[5;2~",     0,    0},
        { XK_KP_Prior,      XK_ANY_MOD,     "\x1b[5~",       0,    0},
        { XK_KP_Begin,      XK_ANY_MOD,     "\x1b[E",        0,    0},
        { XK_KP_End,        ControlMask,    "\x1b[J",       -1,    0},
        { XK_KP_End,        ControlMask,    "\x1b[1;5F",     1,    0},
        { XK_KP_End,        ShiftMask,      "\x1b[K",       -1,    0},
        { XK_KP_End,        ShiftMask,      "\x1b[1;2F",     1,    0},
        { XK_KP_End,        XK_ANY_MOD,     "\x1b[4~",       0,    0},
        { XK_KP_Next,       ShiftMask,      "\x1b[6;2~",     0,    0},
        { XK_KP_Next,       XK_ANY_MOD,     "\x1b[6~",       0,    0},
        { XK_KP_Insert,     ShiftMask,      "\x1b[2;2~",     1,    0},
        { XK_KP_Insert,     ShiftMask,      "\x1b[4l",      -1,    0},
        { XK_KP_Insert,     ControlMask,    "\x1b[L",       -1,    0},
        { XK_KP_Insert,     ControlMask,    "\x1b[2;5~",     1,    0},
        { XK_KP_Insert,     XK_ANY_MOD,     "\x1b[4h",      -1,    0},
        { XK_KP_Insert,     XK_ANY_MOD,     "\x1b[2~",       1,    0},
        { XK_KP_Delete,     ControlMask,    "\x1b[M",       -1,    0},
        { XK_KP_Delete,     ControlMask,    "\x1b[3;5~",     1,    0},
        { XK_KP_Delete,     ShiftMask,      "\x1b[2K",      -1,    0},
        { XK_KP_Delete,     ShiftMask,      "\x1b[3;2~",     1,    0},
        { XK_KP_Delete,     XK_ANY_MOD,     "\x1b[P",       -1,    0},
        { XK_KP_Delete,     XK_ANY_MOD,     "\x1b[3~",       1,    0},
        { XK_KP_Multiply,   XK_ANY_MOD,     "\x1bOj",        2,    0},
        { XK_KP_Add,        XK_ANY_MOD,     "\x1bOk",        2,    0},
        { XK_KP_Enter,      XK_ANY_MOD,     "\x1bOM",        2,    0},
        { XK_KP_Enter,      XK_ANY_MOD,     "\r",           -1,    0},
        { XK_KP_Subtract,   XK_ANY_MOD,     "\x1bOm",        2,    0},
        { XK_KP_Decimal,    XK_ANY_MOD,     "\x1bOn",        2,    0},
        { XK_KP_Divide,     XK_ANY_MOD,     "\x1bOo",        2,    0},
        { XK_KP_0,          XK_ANY_MOD,     "\x1bOp",        2,    0},
        { XK_KP_1,          XK_ANY_MOD,     "\x1bOq",        2,    0},
        { XK_KP_2,          XK_ANY_MOD,     "\x1bOr",        2,    0},
        { XK_KP_3,          XK_ANY_MOD,     "\x1bOs",        2,    0},
        { XK_KP_4,          XK_ANY_MOD,     "\x1bOt",        2,    0},
        { XK_KP_5,          XK_ANY_MOD,     "\x1bOu",        2,    0},
        { XK_KP_6,          XK_ANY_MOD,     "\x1bOv",        2,    0},
        { XK_KP_7,          XK_ANY_MOD,     "\x1bOw",        2,    0},
        { XK_KP_8,          XK_ANY_MOD,     "\x1bOx",        2,    0},
        { XK_KP_9,          XK_ANY_MOD,     "\x1bOy",        2,    0},
        { XK_Up,            ShiftMask,      "\x1b[1;2A",     0,    0},
        { XK_Up,            Mod1Mask,       "\x1b[1;3A",     0,    0},
        { XK_Up,         ShiftMask|Mod1Mask,"\x1b[1;4A",     0,    0},
        { XK_Up,            ControlMask,    "\x1b[1;5A",     0,    0},
        { XK_Up,      ShiftMask|ControlMask,"\x1b[1;6A",     0,    0},
        { XK_Up,       ControlMask|Mod1Mask,"\x1b[1;7A",     0,    0},
        { XK_Up,ShiftMask|ControlMask|Mod1Mask,"\x1b[1;8A",  0,    0},
        { XK_Up,            XK_ANY_MOD,     "\x1b[A",        0,   -1},
        { XK_Up,            XK_ANY_MOD,     "\x1bOA",        0,    1},
        { XK_Down,          ShiftMask,      "\x1b[1;2B",     0,    0},
        { XK_Down,          Mod1Mask,       "\x1b[1;3B",     0,    0},
        { XK_Down,       ShiftMask|Mod1Mask,"\x1b[1;4B",     0,    0},
        { XK_Down,          ControlMask,    "\x1b[1;5B",     0,    0},
        { XK_Down,    ShiftMask|ControlMask,"\x1b[1;6B",     0,    0},
        { XK_Down,     ControlMask|Mod1Mask,"\x1b[1;7B",     0,    0},
        { XK_Down,ShiftMask|ControlMask|Mod1Mask,"\x1b[1;8B",0,    0},
        { XK_Down,          XK_ANY_MOD,     "\x1b[B",        0,   -1},
        { XK_Down,          XK_ANY_MOD,     "\x1bOB",        0,    1},
        { XK_Left,          ShiftMask,      "\x1b[1;2D",     0,    0},
        { XK_Left,          Mod1Mask,       "\x1b[1;3D",     0,    0},
        { XK_Left,       ShiftMask|Mod1Mask,"\x1b[1;4D",     0,    0},
        { XK_Left,          ControlMask,    "\x1b[1;5D",     0,    0},
        { XK_Left,    ShiftMask|ControlMask,"\x1b[1;6D",     0,    0},
        { XK_Left,     ControlMask|Mod1Mask,"\x1b[1;7D",     0,    0},
        { XK_Left,ShiftMask|ControlMask|Mod1Mask,"\x1b[1;8D",0,    0},
        { XK_Left,          XK_ANY_MOD,     "\x1b[D",        0,   -1},
        { XK_Left,          XK_ANY_MOD,     "\x1bOD",        0,    1},
        { XK_Right,         ShiftMask,      "\x1b[1;2C",     0,    0},
        { XK_Right,         Mod1Mask,       "\x1b[1;3C",     0,    0},
        { XK_Right,      ShiftMask|Mod1Mask,"\x1b[1;4C",     0,    0},
        { XK_Right,         ControlMask,    "\x1b[1;5C",     0,    0},
        { XK_Right,   ShiftMask|ControlMask,"\x1b[1;6C",     0,    0},
        { XK_Right,    ControlMask|Mod1Mask,"\x1b[1;7C",     0,    0},
        { XK_Right,ShiftMask|ControlMask|Mod1Mask,"\x1b[1;8C",0,   0},
        { XK_Right,         XK_ANY_MOD,     "\x1b[C",        0,   -1},
        { XK_Right,         XK_ANY_MOD,     "\x1bOC",        0,    1},
        { XK_ISO_Left_Tab,  ShiftMask,      "\x1b[Z",        0,    0},
        { XK_Return,        Mod1Mask,       "\x1b\r",        0,    0},
        { XK_Return,        XK_ANY_MOD,     "\r",            0,    0},
        { XK_Insert,        ShiftMask,      "\x1b[4l",      -1,    0},
        { XK_Insert,        ShiftMask,      "\x1b[2;2~",     1,    0},
        { XK_Insert,        ControlMask,    "\x1b[L",       -1,    0},
        { XK_Insert,        ControlMask,    "\x1b[2;5~",     1,    0},
        { XK_Insert,        XK_ANY_MOD,     "\x1b[4h",      -1,    0},
        { XK_Insert,        XK_ANY_MOD,     "\x1b[2~",       1,    0},
        { XK_Delete,        ControlMask,    "\x1b[M",       -1,    0},
        { XK_Delete,        ControlMask,    "\x1b[3;5~",     1,    0},
        { XK_Delete,        ShiftMask,      "\x1b[2K",      -1,    0},
        { XK_Delete,        ShiftMask,      "\x1b[3;2~",     1,    0},
        { XK_Delete,        XK_ANY_MOD,     "\x1b[P",       -1,    0},
        { XK_Delete,        XK_ANY_MOD,     "\x1b[3~",       1,    0},
        { XK_BackSpace,     XK_NO_MOD,      "\b",            0,    0},
        { XK_BackSpace,     Mod1Mask,       "\x1b\b",      0,    0},
        { XK_Home,          ShiftMask,      "\x1b[2J",       0,   -1},
        { XK_Home,          ShiftMask,      "\x1b[1;2H",     0,    1},
        { XK_Home,          XK_ANY_MOD,     "\x1b[H",        0,   -1},
        { XK_Home,          XK_ANY_MOD,     "\x1b[1~",       0,    1},
        { XK_End,           ControlMask,    "\x1b[J",       -1,    0},
        { XK_End,           ControlMask,    "\x1b[1;5F",     1,    0},
        { XK_End,           ShiftMask,      "\x1b[K",       -1,    0},
        { XK_End,           ShiftMask,      "\x1b[1;2F",     1,    0},
        { XK_End,           XK_ANY_MOD,     "\x1b[4~",       0,    0},
        { XK_Prior,         ControlMask,    "\x1b[5;5~",     0,    0},
        { XK_Prior,         ShiftMask,      "\x1b[5;2~",     0,    0},
        { XK_Prior,         XK_ANY_MOD,     "\x1b[5~",       0,    0},
        { XK_Next,          ControlMask,    "\x1b[6;5~",     0,    0},
        { XK_Next,          ShiftMask,      "\x1b[6;2~",     0,    0},
        { XK_Next,          XK_ANY_MOD,     "\x1b[6~",       0,    0},
        { XK_F1,            XK_NO_MOD,      "\x1bOP" ,       0,    0},
        { XK_F1,            ShiftMask,      "\x1b[1;2P",     0,    0}, -- F13
        { XK_F1,            ControlMask,    "\x1b[1;5P",     0,    0}, -- F25
        { XK_F1,            Mod4Mask,       "\x1b[1;6P",     0,    0}, -- F37
        { XK_F1,            Mod1Mask,       "\x1b[1;3P",     0,    0}, -- F49
        { XK_F1,            Mod3Mask,       "\x1b[1;4P",     0,    0}, -- F61
        { XK_F2,            XK_NO_MOD,      "\x1bOQ" ,       0,    0},
        { XK_F2,            ShiftMask,      "\x1b[1;2Q",     0,    0}, -- F14
        { XK_F2,            ControlMask,    "\x1b[1;5Q",     0,    0}, -- F26
        { XK_F2,            Mod4Mask,       "\x1b[1;6Q",     0,    0}, -- F38
        { XK_F2,            Mod1Mask,       "\x1b[1;3Q",     0,    0}, -- F50
        { XK_F2,            Mod3Mask,       "\x1b[1;4Q",     0,    0}, -- F62
        { XK_F3,            XK_NO_MOD,      "\x1bOR" ,       0,    0},
        { XK_F3,            ShiftMask,      "\x1b[1;2R",     0,    0}, -- F15
        { XK_F3,            ControlMask,    "\x1b[1;5R",     0,    0}, -- F27
        { XK_F3,            Mod4Mask,       "\x1b[1;6R",     0,    0}, -- F39
        { XK_F3,            Mod1Mask,       "\x1b[1;3R",     0,    0}, -- F51
        { XK_F3,            Mod3Mask,       "\x1b[1;4R",     0,    0}, -- F63
        { XK_F4,            XK_NO_MOD,      "\x1bOS" ,       0,    0},
        { XK_F4,            ShiftMask,      "\x1b[1;2S",     0,    0}, -- F16
        { XK_F4,            ControlMask,    "\x1b[1;5S",     0,    0}, -- F28
        { XK_F4,            Mod4Mask,       "\x1b[1;6S",     0,    0}, -- F40
        { XK_F4,            Mod1Mask,       "\x1b[1;3S",     0,    0}, -- F52
        { XK_F5,            XK_NO_MOD,      "\x1b[15~",      0,    0},
        { XK_F5,            ShiftMask,      "\x1b[15;2~",    0,    0}, -- F17
        { XK_F5,            ControlMask,    "\x1b[15;5~",    0,    0}, -- F29
        { XK_F5,            Mod4Mask,       "\x1b[15;6~",    0,    0}, -- F41
        { XK_F5,            Mod1Mask,       "\x1b[15;3~",    0,    0}, -- F53
        { XK_F6,            XK_NO_MOD,      "\x1b[17~",      0,    0},
        { XK_F6,            ShiftMask,      "\x1b[17;2~",    0,    0}, -- F18
        { XK_F6,            ControlMask,    "\x1b[17;5~",    0,    0}, -- F30
        { XK_F6,            Mod4Mask,       "\x1b[17;6~",    0,    0}, -- F42
        { XK_F6,            Mod1Mask,       "\x1b[17;3~",    0,    0}, -- F54
        { XK_F7,            XK_NO_MOD,      "\x1b[18~",      0,    0},
        { XK_F7,            ShiftMask,      "\x1b[18;2~",    0,    0}, -- F19
        { XK_F7,            ControlMask,    "\x1b[18;5~",    0,    0}, -- F31
        { XK_F7,            Mod4Mask,       "\x1b[18;6~",    0,    0}, -- F43
        { XK_F7,            Mod1Mask,       "\x1b[18;3~",    0,    0}, -- F55
        { XK_F8,            XK_NO_MOD,      "\x1b[19~",      0,    0},
        { XK_F8,            ShiftMask,      "\x1b[19;2~",    0,    0}, -- F20
        { XK_F8,            ControlMask,    "\x1b[19;5~",    0,    0}, -- F32
        { XK_F8,            Mod4Mask,       "\x1b[19;6~",    0,    0}, -- F44
        { XK_F8,            Mod1Mask,       "\x1b[19;3~",    0,    0}, -- F56
        { XK_F9,            XK_NO_MOD,      "\x1b[20~",      0,    0},
        { XK_F9,            ShiftMask,      "\x1b[20;2~",    0,    0}, -- F21
        { XK_F9,            ControlMask,    "\x1b[20;5~",    0,    0}, -- F33
        { XK_F9,            Mod4Mask,       "\x1b[20;6~",    0,    0}, -- F45
        { XK_F9,            Mod1Mask,       "\x1b[20;3~",    0,    0}, -- F57
        { XK_F10,           XK_NO_MOD,      "\x1b[21~",      0,    0},
        { XK_F10,           ShiftMask,      "\x1b[21;2~",    0,    0}, -- F22
        { XK_F10,           ControlMask,    "\x1b[21;5~",    0,    0}, -- F34
        { XK_F10,           Mod4Mask,       "\x1b[21;6~",    0,    0}, -- F46
        { XK_F10,           Mod1Mask,       "\x1b[21;3~",    0,    0}, -- F58
        { XK_F11,           XK_NO_MOD,      "\x1b[23~",      0,    0},
        { XK_F11,           ShiftMask,      "\x1b[23;2~",    0,    0}, -- F23
        { XK_F11,           ControlMask,    "\x1b[23;5~",    0,    0}, -- F35
        { XK_F11,           Mod4Mask,       "\x1b[23;6~",    0,    0}, -- F47
        { XK_F11,           Mod1Mask,       "\x1b[23;3~",    0,    0}, -- F59
        { XK_F12,           XK_NO_MOD,      "\x1b[24~",      0,    0},
        { XK_F12,           ShiftMask,      "\x1b[24;2~",    0,    0}, -- F24
        { XK_F12,           ControlMask,    "\x1b[24;5~",    0,    0}, -- F36
        { XK_F12,           Mod4Mask,       "\x1b[24;6~",    0,    0}, -- F48
        { XK_F12,           Mod1Mask,       "\x1b[24;3~",    0,    0}, -- F60
        { XK_F13,           XK_NO_MOD,      "\x1b[1;2P",     0,    0},
        { XK_F14,           XK_NO_MOD,      "\x1b[1;2Q",     0,    0},
        { XK_F15,           XK_NO_MOD,      "\x1b[1;2R",     0,    0},
        { XK_F16,           XK_NO_MOD,      "\x1b[1;2S",     0,    0},
        { XK_F17,           XK_NO_MOD,      "\x1b[15;2~",    0,    0},
        { XK_F18,           XK_NO_MOD,      "\x1b[17;2~",    0,    0},
        { XK_F19,           XK_NO_MOD,      "\x1b[18;2~",    0,    0},
        { XK_F20,           XK_NO_MOD,      "\x1b[19;2~",    0,    0},
        { XK_F21,           XK_NO_MOD,      "\x1b[20;2~",    0,    0},
        { XK_F22,           XK_NO_MOD,      "\x1b[21;2~",    0,    0},
        { XK_F23,           XK_NO_MOD,      "\x1b[23;2~",    0,    0},
        { XK_F24,           XK_NO_MOD,      "\x1b[24;2~",    0,    0},
        { XK_F25,           XK_NO_MOD,      "\x1b[1;5P",     0,    0},
        { XK_F26,           XK_NO_MOD,      "\x1b[1;5Q",     0,    0},
        { XK_F27,           XK_NO_MOD,      "\x1b[1;5R",     0,    0},
        { XK_F28,           XK_NO_MOD,      "\x1b[1;5S",     0,    0},
        { XK_F29,           XK_NO_MOD,      "\x1b[15;5~",    0,    0},
        { XK_F30,           XK_NO_MOD,      "\x1b[17;5~",    0,    0},
        { XK_F31,           XK_NO_MOD,      "\x1b[18;5~",    0,    0},
        { XK_F32,           XK_NO_MOD,      "\x1b[19;5~",    0,    0},
        { XK_F33,           XK_NO_MOD,      "\x1b[20;5~",    0,    0},
        { XK_F34,           XK_NO_MOD,      "\x1b[21;5~",    0,    0},
        { XK_F35,           XK_NO_MOD,      "\x1b[23;5~",    0,    0},
    },

    --[[
        Selection types' masks.
        Use the same masks as usual.
        Button1Mask is always unset to make masks match between ButtonPress,
            ButtonRelease, and ButtonNotify.
        If no match is found, then regular selection is used.
    --]]
    selmasks = {
        [SEL_RECTANGULAR] = Mod1Mask,
    },

    -- Printable characters in ASCII, used to estimate the advance width of single wide characters.
    ascii_printable = " !\"#$%&'()* ,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
}
