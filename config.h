/* See LICENSE file for copyright and license details. */

#include <stdlib.h>

#define DYN_STRUCT(ident, ty) \
	typedef struct ident {   \
		ty *ptr;             \
		size_t size;         \
	} ident;

DYN_STRUCT(DynString, char);

static char *font;
static int borderpx;

static char *shell;
char *utmp;
char *stty_args;

char *vtiden;

static float cwscale;
static float chscale;

wchar_t *worddelimiters;

static unsigned int doubleclicktimeout;
static unsigned int tripleclicktimeout;

int allowaltscreen;

static unsigned int xfps;
static unsigned int actionfps;

static unsigned int blinktimeout;

static unsigned int cursorthickness;

static int bellvolume;

char *termname;

unsigned int tabspaces;

DYN_STRUCT(Colorname, char *);
static Colorname colorname;

unsigned int defaultfg;
unsigned int defaultbg;
static unsigned int defaultcs;
static unsigned int defaultrcs;

static unsigned int cursorshape;

static unsigned int cols;
static unsigned int rows;

static unsigned int mouseshape;
static unsigned int mousefg;
static unsigned int mousebg;

static unsigned int defaultattr;

static uint forcemousemod;

DYN_STRUCT(MouseShortcuts, MouseShortcut);
static MouseShortcuts mshortcuts;

static int modkey;
static int termmod = (ControlMask | ShiftMask);

DYN_STRUCT(Shortcuts, Shortcut);
static Shortcuts shortcuts;

DYN_STRUCT(MappedKeys, KeySym);
static MappedKeys mappedkeys;

static uint ignoremod;

DYN_STRUCT(Keys, Key);
static Keys key;

DYN_STRUCT(SelMasks, uint);
static SelMasks selmasks;

static char *ascii_printable;
