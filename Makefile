# lst - simple terminal
# See LICENSE file for copyright and license details.
.POSIX:

include config.mk

SRC = lst.c _x.c
OBJ = $(SRC:.c=.o)

all: options lst

options:
	@echo lst build options:
	@echo "CFLAGS  = $(STCFLAGS)"
	@echo "LDFLAGS = $(STLDFLAGS)"
	@echo "CC      = $(CC)"

# .c.o:
# 	$(CC) $(STCFLAGS) -c $<

lst.o: config.h lst.h win.h
	$(CC) $(STCFLAGS) -c lst.c

_x.o: arg.h config.h lst.h win.h
	sed "s+INSTALL_DIR+$(DESTDIR)$(PREFIX)+g" < x.c > _x.c
	$(CC) $(STCFLAGS) -c _x.c

$(OBJ): config.h config.mk

lst: $(OBJ)
	$(CC) -o $@ $(OBJ) $(STLDFLAGS)
	rm -f _x.c
	mv _x.o x.o

clean:
	rm -f lst $(OBJ) x.o lst-$(VERSION).tar.gz

dist: clean
	mkdir -p lst-$(VERSION)
	cp -R FAQ LEGACY TODO LICENSE Makefile README config.mk\
		lst.info lst.1 arg.h lst.h win.h $(SRC) lst-$(VERSION)
	tar -cf - lst-$(VERSION) | gzip > lst-$(VERSION).tar.gz
	rm -rf lst-$(VERSION)

install: lst
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f lst $(DESTDIR)$(PREFIX)/bin/
	mkdir -p $(DESTDIR)$(PREFIX)/lib/lst
	cp -f helper.lua bindings.lua $(DESTIDIR)$(PREFIX)/lib/lst
	mkdir -p /etc/xdg/lst
	cp -f config.lua /etc/xdg/lst
	chmod 755 $(DESTDIR)$(PREFIX)/bin/lst
	chmod 755 $(DESTDIR)$(PREFIX)/lib/lst/helper.lua
	chmod 755 $(DESTDIR)$(PREFIX)/lib/lst/bindings.lua
	mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	sed "s/VERSION/$(VERSION)/g" < lst.1 > $(DESTDIR)$(MANPREFIX)/man1/lst.1
	chmod 644 $(DESTDIR)$(MANPREFIX)/man1/lst.1
	tic -sx lst.info
	@echo Please see the README file regarding the terminfo entry of lst.

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/lst
	rm -rf $(DESTDIR)$(PREFIX)/lib/lst
	rm -f $(DESTDIR)$(MANPREFIX)/man1/lst.1

.PHONY: all options clean dist install uninstall
